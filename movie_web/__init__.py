import os

from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'movies.sqlite'),
    )

    if test_config is not None:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    else:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
        os.makedirs(app.config.POSTER_DIR)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    with app.app_context():
        db.init_db()

    from . import auth
    app.register_blueprint(auth.bp)

    from . import movies
    app.register_blueprint(movies.bp)
    app.add_url_rule('/', endpoint='index')

    return app
