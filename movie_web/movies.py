import os
import sys
import shutil
import requests
from requests_html import HTMLSession

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from movie_web import config
from movie_web.auth import login_required
from movie_web.db import get_db

bp = Blueprint('movies', __name__)

@bp.route('/')
def index():
    db = get_db()
    movies = db.execute(
        'SELECT m.id, title, imdb_id, description, view_date, author_id, \
        create_date, u.username FROM movies m JOIN user u ORDER BY create_date DESC'
    ).fetchall()
    return render_template('movie/index.html', movies=movies)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        imdb_id = request.form['imdb_id']
        view_date = request.form['view_date']
        error = None

        if not title:
            error = 'Title is required.'

        if not imdb_id:
            error = 'IMDB ID is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()

            session = HTMLSession()
            i = session.get('https://imdb.com/title/{}/'.format(imdb_id))

            for meta in i.html.find('meta'):
                if 'description' in meta.html:
                    description = meta.attrs['content']
                    break

            if not view_date:
                db.execute('INSERT INTO movies (title, imdb_id, description, \
                           author_id) VALUES (?, ?, ?, ?)',
                           (title, imdb_id, description, g.user['id'])
                )
            else:
                db.execute('INSERT INTO movies (title, imdb_id, description, \
                           author_id, view_date) VALUES (?, ?, ?, ?, ?)',
                           (title, imdb_id, description, g.user['id'], view_date)
                )
            db.commit()

        # download movie poster
        if not os.path.exists(os.path.join('/srv/movie_web', 'static', 'posters',
                                           '{}.jpg'.format(imdb_id))):
            for img in i.html.find('img'):
                if 'Poster' in img.html:
                    r = requests.get(img.attrs['src'], stream=True)
                    if r.status_code == 200:
                        with open(os.path.join('/srv/movie_web', 'static',
                                               'posters', '{}.jpg'
                                               .format(imdb_id)), 'wb') as f:
                            r.raw.decode_content = True
                            shutil.copyfileobj(r.raw, f)

        return redirect(url_for('movies.index'))

    return render_template('movie/create.html')


def get_movie(id, check_author=True):
    movie = get_db().execute(
        'SELECT p.id, title, imdb_id, description, view_date, create_date, author_id, username'
        ' FROM movies p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if movie is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and movie['author_id'] != g.user['id']:
        abort(403)

    return movie


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    movie = get_movie(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for(' movies.index'))

    return render_template('movie/update.html', movie=movie)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_movie(id)
    db = get_db()
    db.execute('DELETE FROM movies WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('movies.index'))
