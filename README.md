# Movie-web
This project is a web site to allow easy scheduling of movie viewing for a group.

## Building
Standard docker build rules

```bash
$ docker build -t movie-web:1.0 .
```

## Configuration
if a config.py file exists in the current directory it will be loaded.

### Config options
* SECRET_KEY - flask debugging key
* DATABASE - where to store the database

## Running
Execute the docker container

```bash
$ docker run --rm -v /path/to/posters:/srv/posters \
  -v /path/to/db:/DATABASE -p 80:5000 \
  movie_web:1.0
```

Visit http://localhost:8000/ and choose 'register' to create an account.
Once your account is created, login and choose the 'new' link to create
an entry.
