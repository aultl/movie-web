FROM python:3.6-alpine

RUN apk add -U libxml2 libxml2-dev python-dev tini build-base libxslt-dev

WORKDIR /srv

COPY movie_web/ /srv/movie_web
COPY requirements.txt /srv
COPY config.py /srv/movie_web

RUN mkdir /srv/posters && \
    ln -s /srv/posters /srv/movie_web/static/posters && \
    pip install -r /srv/requirements.txt

EXPOSE 8080

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["waitress-serve", "--call", "movie_web:create_app"]
